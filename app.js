const express = require("express");
const { set } = require("express/lib/application");
const path = require("path");
const app = express();
const PORT = process.env.PORT || 4000;

const server = app.listen(PORT, () => {
  console.log("Chat is on Port ------>", PORT);
});
const io = require("socket.io")(server);

let socketConnected = new Set();

io.on("connection", onConnected);

function onConnected(socket) {
  console.log(socket.id);
  socketConnected.add(socket.id);

  io.emit("clients-total", socketConnected.size);
  // To check the disconnected socket
  socket.on("disconnect", () => {
    console.log("Socket Disconnected---->", socket.id);
    socketConnected.delete(socket.id);
    io.emit("clients-total", socketConnected.size);
  });
}
app.use(express.static(path.join(__dirname, "Public")));
